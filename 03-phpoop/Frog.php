<?php 

class Frog extends Animal{
    public $jump = "Hop Hop";

    public function __construct ($name)
    {
       $this->name = $name;
    }

    public function jump ()
    {
        echo $this->jump;
    }
}

?>