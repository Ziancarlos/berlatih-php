<?php 
require 'Aninmal.php';
require 'Ape.php';
require 'Frog.php';

// realese 0
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"\
echo "<br>";
echo $sheep->legs; // 4
echo "<br>";
echo $sheep->cold_blooded; // "no"
echo "<br>";
// -----------------------
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

echo "<br>";
echo "<br>";

// realese 1
// OUTPUT AKHIR

echo "OUTPUT AKHIR<br>";

echo "<br>";
echo "Name : " . $sheep->name;
echo "<br>";
echo "Legs : " . $sheep->legs;
echo "<br>";
echo "cold blooded : " . $sheep->cold_blooded;

echo "<br>";
echo "<br>";

echo "Name : " . $kodok->name;
echo "<br>";
echo "Legs : " . $kodok->legs;
echo "<br>";
echo "cold blooded : " . $kodok->cold_blooded;
echo "<br>";
echo "Jump : " ;
$kodok->jump();

echo "<br>";
echo "<br>";

echo "Name : " . $sungokong->name;
echo "<br>";
echo "Legs : " . $sungokong->legs;
echo "<br>";
echo "cold blooded : " . $sungokong->cold_blooded;
echo "<br>";
echo "Yell : ";
$sungokong->yell()
   
?>